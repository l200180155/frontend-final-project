import React from "react";
import NavbarMenu from "./Navbar";
import CategoryMenu from "./category";
import ButtonDaftarJual from "./button";
import ListDiminati from "./list_daftar_jual/listDiminati";
import ListProduk from "./list_daftar_jual/list";
import ListTerjual from "./list_daftar_jual/listTerjual";


function DaftarJualMenu() {
    const [list, setList] = React.useState("produk")
  console.log(list==="produk");
    return (
      <div>
        <NavbarMenu/>
        <div>
          <CategoryMenu />
          <div className="container mt-3">
              <div className="mx-60">
                  <div className="mx-60">
                      <div className="row">
                        <ButtonDaftarJual setList={setList} clicked={list}/>
                        {list === "produk"? <ListProduk/>: list === "diminati"? <ListDiminati/>: list === "terjual"? <ListTerjual/> : <ListProduk/>} 
                      </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
    );
  }
  
  export default DaftarJualMenu;
