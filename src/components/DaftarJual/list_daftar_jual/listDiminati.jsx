import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Col, Row } from "reactstrap";
import { BsPlusLg } from "react-icons/bs";
import { Link } from "react-router-dom";


const ListDiminati = () => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserOrder = await axios.get(
                    "http://localhost:3200/api/product/sale-list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setOrders(item.data.data))
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    console.log(orders);
    return (
        <Col md={8}>
            <Row>
                {orders.map((orders) => (

                    <Col md={4} className="mb-2 width-50">
                        <Link to={`/info-penawaran`}>
                            <Card key={orders.id}>
                                <CardImg
                                    alt="image produk diminati"
                                    src={orders.image}
                                    top
                                    width="100%"
                                />
                                <CardBody>
                                    <CardTitle tag="h6">
                                        {orders.name}
                                    </CardTitle>
                                    <CardSubtitle
                                        className="mb-2 text-muted"
                                        tag="p"
                                    >
                                        {orders.description}
                                    </CardSubtitle>
                                    <CardText tag="h6">
                                        {orders.price}
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>

                ))}
            </Row>
        </Col>
    )
}
export default ListDiminati