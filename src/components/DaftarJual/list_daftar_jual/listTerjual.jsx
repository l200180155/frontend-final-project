import React from "react";
import { Button, Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Col, Row } from "reactstrap";
import { BsBox, BsChevronRight, BsCurrencyDollar, BsHeart, BsPlusLg } from "react-icons/bs";
import { Link } from "react-router-dom";


const ListTerjual = () => {
    return (
        <Col md={8}>
            <Row>
                {/* {products.map((product) => ())} */}
               
                <Col md={4} className="mb-2 width-50">
                    <Card>
                        <CardImg
                            alt="Card image cap"
                            src="https://picsum.photos/318/180"
                            top
                            width="100%"
                        />
                        <CardBody>
                            <CardTitle tag="h6">
                                Jam Tangan Cassiozzzz
                            </CardTitle>
                            <CardSubtitle
                                className="mb-2 text-muted"
                                tag="p"
                            >
                                Aksesoris
                            </CardSubtitle>
                            <CardText tag="h6">
                                Rp 250.000
                            </CardText>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Col>
    )
}
export default ListTerjual