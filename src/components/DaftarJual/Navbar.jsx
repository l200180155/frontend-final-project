import React, { useEffect, useState } from "react";
import axios from "axios";
import { BsBell, BsListUl } from "react-icons/bs";
import { BiUser } from "react-icons/bi";
import { Card, CardBody, CardSubtitle, CardTitle, Collapse, Input, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem } from "reactstrap";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";


const NavbarMenu = () => {

    const [modal, setmodal] = useState(false)
    const handleOpen = () => {
        setmodal(true)
    }
    const handleClose = () => {
        setmodal(false)
    }

    const [products, setProducts] = useState([]);
    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserRequest = await axios.get(
                    "http://localhost:3200/api/product/sale-list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setProducts(item.data.data))
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    console.log(products);
    return (
        <div className="none" >
            <div  >
                <Navbar
                    color="light"
                    expand="md"
                    light>
                    <NavbarBrand href="/">
                        <img src="img/logo.png" alt="" />
                    </NavbarBrand>
                    <NavItem className="d-flex">
                        <div class="home-search">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M20.9999 20.9999L16.6499 16.6499" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <input type="text" placeholder="Cari di sini ..."/>
                        </div>
                    </NavItem>
                    <NavbarToggler
                        className="me-2"
                        onClick={function noRefCheck() { }}
                    />
                    <Collapse navbar style={{ justifyContent: "end" }}>
                        <Nav navbar >
                            <NavItem>
                                <Link to="/daftarjual">
                                    <BsListUl style={{ fontSize: "20px" }} className="purplee" />
                                </Link>
                            </NavItem>
                            <NavItem onClick={handleOpen}>

                                <BsBell style={{ fontSize: "20px", marginLeft: "20px", cursor: "pointer" }} className="purplee" />

                            </NavItem>
                            <NavItem>
                                <Link to="/akun">
                                    <BiUser style={{ fontSize: "20px", marginLeft: "20px" }} className="purplee" />
                                </Link>
                            </NavItem>
                        </Nav>
                    </Collapse>
                    <Modal show={modal} className="modala" >
                        <Modal.Header onClick={handleClose} closeButton>
                            <Modal.Title>Notification</Modal.Title>
                        </Modal.Header>
                        <Modal.Body className="modal-backdrop.showa">
                            <Card >
                                <CardBody>
                                    {products.map((product) => (
                                        <Link to="/info-penawaran">
                                            <div className="d-flex mb-4 " key={product.id} style={{ justifyContent: "space-between", height: "60px" }}>
                                                <div className="d-flex ">
                                                    <img src={product.image} alt="" width="100px" />
                                                    <div className="ms-2">
                                                        <CardSubtitle className="mb-2 text-muted" tag="h7">
                                                            {product.description}
                                                        </CardSubtitle>
                                                        <div>
                                                            <CardTitle tag="h6">
                                                                {product.name}
                                                            </CardTitle>
                                                            <CardTitle tag="h6">
                                                                {product.price}
                                                            </CardTitle>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    ))}
                                </CardBody>
                            </Card>
                        </Modal.Body>
                    </Modal>
                </Navbar>
            </div >
        </div >
    )
}
export default NavbarMenu;