import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import kado from "../../img/kado.png";
function CarouselHome() {
  const settings = {
    arrows: false,
    infinite: true,
    autoplay: true,
    speed: 4000,
    autoplaySpeed: 4000,
    centerMode: true,
    draggable: true,
    centerPadding: '300px',
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      { breakpoint: 500, settings: { autoplay: true, slidesToShow: 1 } },
    ],
  };
  return (
    <div>
      {/* <div className="img-banner-responsive"></div>
      <Slider {...settings} className="carousel-home">
        <img
          src="img/img-banner.png"
          alt=""
          className="img-banner border-radius"
        />
        <img
          src="img/img-banner.png"
          alt=""
          className="img-banner border-radius"
        />
        <img
          src="img/img-banner.png"
          alt=""
          className="img-banner border-radius"
        />
      </Slider> */}
        <div className="home-slider">
          <div className="slider1"></div>
          <div className="slider2">
              <div className="slider2-body">
                  <div className="slider2-info">
                      <h1 className="home-title">Bulan Ramadhan<br/>Banyak diskon!</h1>
                      <h5>Diskon Hingga</h5>
                      <h1 className="disc">60%</h1>
                  </div>
                  <img className="kado" src={kado} alt="Kado"/>
                  <div className="imagebg"></div>
              </div>
          </div>
          <div className="slider3"></div>
      </div>
    </div>
  );
}
export default CarouselHome;
