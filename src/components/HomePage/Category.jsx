// import { Button } from 'reactstrap';
// import { FiSearch } from 'react-icons/fi';
// import Slider from 'react-slick';
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';
// import { useState } from 'react';
// function Category(props) {
//   var settings = {
//     infinite: false,
//     speed: 500,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     initialSlide: 0,
//     className: 'slider variable-width',
//     variableWidth: true,
//     responsive: [
//       {
//         breakpoint: 1024,
//         settings: {
//           slidesToShow: 3,
//           slidesToScroll: 3,
//           infinite: true,
//           dots: true,
//         },
//       },
//       {
//         breakpoint: 600,
//         settings: {
//           slidesToShow: 2,
//           slidesToScroll: 2,
//           initialSlide: 2,
//         },
//       },
//       {
//         breakpoint: 480,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//         },
//       },
//     ],
//   };

//   const [listProduct, setListProduct] = useState(props.products);
//   const filterResult = (item) => {
//     const result = props.products.filter((curData) => {
//       return curData.categoryId === item;
//     });
//     setListProduct(result);
//   };
//   return (
//     <div className="container">
//       <h5 className="fw-bold">Telusuri Kategori</h5>
//       <Slider {...settings} className="carousel-category">
//         <div className="item-kategori m-3 ">
//           <Button className="btn-kategori border-radius" style={{ width: 150 }}>
//             <FiSearch /> Semua
//           </Button>{' '}
//         </div>
//         <div className="item-kategori m-3 ">
//           <Button
//             className="btn-kategori border-radius"
//             onClick={() => filterResult(2)}
//           >
//             <FiSearch /> Hobi
//           </Button>{' '}
//         </div>
//         <div className="item-kategori m-3 ">
//           <Button className="btn-kategori border-radius">
//             <FiSearch /> Kendaraan
//           </Button>{' '}
//         </div>
//         <div className="item-kategori m-3 ">
//           <Button className="btn-kategori border-radius">
//             <FiSearch /> Baju
//           </Button>{' '}
//         </div>
//         <div className="item-kategori m-3 ">
//           <Button className="btn-kategori border-radius">
//             <FiSearch /> Elektronik
//           </Button>{' '}
//         </div>
//         <div className="item-kategori m-3 ">
//           <Button className="btn-kategori border-radius">
//             <FiSearch /> Kesehatan
//           </Button>{' '}
//         </div>
//       </Slider>
//     </div>
//   );
// }
// export default Category;
