import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,NavbarToggler,Collapse,Nav,NavItem,NavLink,NavbarBrand,Input,} from 'reactstrap';
import { MdLogin } from 'react-icons/md';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { BsBell, BsListUl } from "react-icons/bs";
import { FiUser } from 'react-icons/fi';
import { AiOutlineUnorderedList } from 'react-icons/ai';
import { IoIosNotificationsOutline } from 'react-icons/io';
import CardProduct from './CardProduct';
import SimpleSlider from './SimpleSlider ';
import { BiUser } from "react-icons/bi";


function NavbarHome() {
  const [isOpen, setIsOpen] = useState(false);
  const [refetch, setRefetch] = useState([]);
  const [isLoggedin, setIsLoggedin] = useState(false);
  const [modalLogout, setModalLogout] = useState(false);

  const currentUser = async () => {
    try {
      const response = await axios.get(
        'http://localhost:3200/api/user/current-user',
        {
          headers: { authorization: localStorage.getItem('token') },
        }
      );
      setRefetch(response.data.data);
      setIsLoggedin(true);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => currentUser, []);

  const nav = useNavigate();
  const logout = () => {
    localStorage.removeItem('token');
    setIsLoggedin(false);
    nav('/login');
  };

  function handleModalLogout() {
    setModalLogout(!modalLogout);
  }

  const [products, setProduct] = useState([]);
  const [filteredProduct, setFilteredProduct] = useState([]);

  const getProductsData = async () => {
    try {
      const response = await axios.get('http://localhost:3200/api/product/list');
      setProduct(response.data.data);
      setFilteredProduct(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const [searchInput, setSearchInput] = useState('');
  const filterResult = (productParam) => {
    setFilteredProduct(
      products.filter((product) => product.categoryId === productParam)
    );
    setSearchInput(productParam);
    if (searchInput !== '') {
      const filteredData = products.filter((item) => {
        return Object.values(item)
          .join('')
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredProduct(filteredData);
    } else {
      setFilteredProduct(products);
    }
  };
  useEffect(() => getProductsData, []);
  return (
    <div>
      <Navbar className="navbar-homepage " expand="lg" light container>
        <NavbarBrand href="/">
          <img src="img/logo.png" alt="" />
        </NavbarBrand>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}
          className="me-auto border-radius"
        />
        <div class="home-search">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M20.9999 20.9999L16.6499 16.6499" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
            <input type="text" placeholder="Cari di sini ..."/>
        </div>
        <Collapse isOpen={isOpen} navbar style={{ justifyContent: "end" }}>
          <Nav className="" navbar>
            <NavItem>
              {isLoggedin ? (
                <Nav className="" navbar>
                  <NavItem>
                    <NavLink href="/daftarjual">
                      <BsListUl style={{ fontSize: "20px" }} className="purplee" />
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#" id="PopoverClick" type="button">
                      <BsBell style={{ fontSize: "20px", marginLeft: "20px", cursor: "pointer" }} className="purplee" />
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#" onClick={handleModalLogout}>
                      <BiUser style={{ fontSize: "20px", marginLeft: "20px" }} className="purplee" />
                    </NavLink>
                  </NavItem>
                </Nav>
              ) : (
                <Link to="/login" className="btn btn-purple border-radius p-2">
                  <MdLogin /> Masuk
                </Link>
              )}
            </NavItem>
          </Nav>
        </Collapse>
        {modalLogout && (
          <div
            className="card-atas position-absolute  "
            style={{
              width: '100px',
              height: '50px',
              right: '70px',
              top: '60px',
            }}
            onClick={logout}
          >
            <p className="text-center logout-pointer">
              <b>Logout</b>
            </p>
          </div>
        )}
      </Navbar>
      <SimpleSlider />
      <CardProduct
        products={products}
        filteredProduct={filteredProduct}
        setFilteredProduct={setFilteredProduct}
        filterResult={filterResult}
      />
    </div>
  );
}

export default NavbarHome;
