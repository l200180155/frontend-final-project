import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,NavbarToggler,Collapse,Nav,NavItem,Form,Input,NavLink,NavbarBrand,} from 'reactstrap';
import { BiUser } from "react-icons/bi";
import { BsBell, BsListUl } from "react-icons/bs";
import { AiOutlineUnorderedList } from 'react-icons/ai';
import { FiUser } from 'react-icons/fi';
import { IoIosNotificationsOutline } from 'react-icons/io';
import { Link } from 'react-router-dom';
import NotifikasiTawar from './NotifikasiTawar';
import logo from "../images/Rectangle-127.png";

function NavbarProduct() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <Navbar expand="lg" light container className="shadow-sm mb-3 navbar-seller">
        <NavbarBrand href="/">
          <img src={logo} alt="logo" />
        </NavbarBrand>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}/>
        <Collapse isOpen={isOpen} navbar>
          <Form className="me-auto">
          <div class="home-search">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
              <path d="M20.9999 20.9999L16.6499 16.6499" stroke="#8A8A8A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
            <input type="text" placeholder="Cari di sini ..."/>
          </div>
          </Form>
          <Nav className="ms-auto" navbar>
            <NavItem>
              <NavLink href="/daftarjual">
                <BsListUl style={{ fontSize: "20px" }} className="purplee" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#" id="PopoverClick" type="button">
                <BsBell style={{ fontSize: "20px", marginLeft: "20px", cursor: "pointer" }} className="purplee" />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/akun">
                <BiUser style={{ fontSize: "20px", marginLeft: "20px" }} className="purplee" />
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <NotifikasiTawar />
    </div>
  );
}

export default NavbarProduct;
