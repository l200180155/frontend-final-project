import { useState } from 'react';
import {Button,Modal,ModalHeader,ModalBody,ModalFooter,UncontrolledAlert,} from 'reactstrap';
import CardUser from './CardUser';
import "./StyleBuyer.css";

function CardProduct({ detailProduct, detailUser }) {
  const [modal, setModal] = useState(false);

  const toggleModal = () => setModal(!modal);

  return (
    <div>
      <div className="card">
        <div className="card-body card-konten">
          <h5 className="">{detailProduct.name}</h5>
          <h6 className="card-subtitle mb-2">
            {detailProduct.categoryId}
          </h6>
          <p className="card-text">Rp. {detailProduct.price}</p>
          <button
            className="card-link btn btn-terbit w-100 mb-3"
            onClick={toggleModal}
          >
            Saya Tertarik dan ingin Nego
          </button>
        </div>
      </div>
      <CardUser detailProduct={detailProduct} detailUser={detailUser} />
      {/* modal tawar */}
      <Modal centered isOpen={modal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Masukkan Harga Tawarmu</ModalHeader>
        <ModalBody>
          Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan
          segera dihubungi penjual.
          <div className="nama-penjual bg-grey">
            <div className="row g-0">
              <div className="col-md-4 me-2">
                <img
                  src={detailUser.image}
                  className="img-fluid rounded-start"
                  alt="penjual"
                />
              </div>
              <div className="col-md-7">
                <div className="identitas">
                  <p className="f-14">{detailUser.name}</p>
                  <p className="kota f-12">
                    <small className="text-muted">{detailUser.address}</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">
              Harga tawar
            </label>
            <input type="text" class="form-control" placeholder="Rp. 0,00" />
          </div>
          <Button className="btn btn-dark btn-block fa-lg mb-2 pt-2 pb-2" toggle={toggleModal}>
            Kirim
          </Button>
        </ModalBody>
      </Modal>
      {/* validasi kirim */}
      <UncontrolledAlert className="bg-success alert-tawar">
        Harga tawarmu berhasil dikirim ke penjual
      </UncontrolledAlert>
    </div>
  );
}
export default CardProduct;
