import "./StyleBuyer.css";
function CardUser({ detailUser }) {
  return (
    <div>
      <div className="nama-penjual">
        <div className="row g-0">
          <div className="col-md-4 me-2">
            <img
              src={detailUser.image}
              className="img-fluid rounded-start"
              alt="penjual"
            />
          </div>
          <div className="col-md-7">
            <div className="identitas">
              <p className="f-14">{detailUser.name}</p>
              <p className="kota f-12">
                <small className="text-muted">{detailUser.address}</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardUser;
