import React from 'react';

const Button = ({title, ...rest}) => {
    return (
        <button type="submit" className="btn btn-dark btn-block fa-lg mb-3" {...rest}>{title}</button>
    )
}

export default Button