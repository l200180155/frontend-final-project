import React from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import arrow from '../img/arrow.png';
// import foto from '../img/foto.png';
import pensil from './icons/pensil.png';

const ProfilSection = () => (
    <div>
        <div className='profil-section'>
            <div class="container">
                <div className='menu-profil'>
                    <Row>
                        <Col sm="4">
                            <button className='profil-button'></button>
                            <div className='button-back2'>
                                <img src={arrow} alt="" />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>

        <div className='container'>
            <div className='button-back'>
                <a href="/"><img src={arrow} alt="" /></a>
            </div>
            <div className='content-profil'>
                <div className='foto-profil'>
                    <img src="" alt="" className='img-responsive profil-img'/>
                </div>

                <div className='konten'>
                    <div className='row'>
                        <p class="card-title mb-4">Kategori</p>
                        <div className='d-flex'>
                        <i class="bi bi-pencil"></i>
                        <a href="info-profil" className='link-category'><p class="card-text category">Ubah Akun</p></a>
                        </div><hr/>
                        <div className='d-flex'>
                        <i class="bi bi-gear"></i>
                        <a href="#" className='link-category'><p class="card-text category">Pengaturan Akun</p></a>
                        </div><hr/>
                        <div className='d-flex'>
                        <i class="bi bi-box-arrow-right"></i>
                        <a href="/" className='link-category' ><p class="card-text category">Keluar</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default ProfilSection;