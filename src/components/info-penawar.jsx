import React from 'react';
import { Row, Col, Modal, ModalHeader , ModalFooter, ModalBody,  Button } from 'reactstrap';
import arrow from '../img/arrow.png';
import seller from '../img/seller.png';
import { useFilePicker } from 'use-file-picker';
import clock1 from "./images/clock1.png";
import "./Buyer/StyleBuyer.css";


function Success(){
    return (
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> This alert box could indicate a successful or positive action.
      </div>
    );
  }
  
  class InfoPenawar extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
          modal: false,
          disabled: false,
          value: 'Terima'
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal,
        });
      }
      handleChange = (e) => {
        if(e.target.value.length >= 1) {
          this.setState({
            disabled: true,
            value: 'Hubungi di'
            
          });
        }
        else{
          this.setState({
            disabled: false
          });
        }
    }
  
      
      render() {

// const InfoPenawar = () => {
    return (
        <div>
            <div className='profil-section'>
                <div class="container">
                    <div className='menu-profil'>
                        <Row>
                            <Col sm="4">
                                <button className='profil-button'></button>
                                <div className='button-back2'>
                                    <img src={arrow} alt="" />
                                </div>
                            </Col>
                            <Col className=" text-center" sm="4">
                                <p>Info Penawar</p>
                            </Col>
                            <Col sm="4">
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>

        <div className='container'>
            <div className='button-back'>
                <a href="/"><img src={arrow} alt="" /></a>
            </div>
            <div className='konten-penawar'>
                <div className='nama-penjual'>
                    <div>
                    <img src={seller} alt="" />
                    </div>
                    <div className='identitas'>
                    <p className='f-14'><strong>Nama Penjual</strong></p>
                    <p className='kota f-12'>kota</p>
                    </div>
                </div>

                <p>Daftar Produkmu yang Ditawar</p>
                <div className='d-flex'>
                    <img src={seller} alt="" className='img-penawar' />
                    <div className='caption'>
                        <p className='f-10'>Penawaran produk</p>
                        <p className='f-14 top-10'>Jam Tangan Casio</p>
                        <p className='f-14 top-10'>Rp 250.000</p>
                        <p className='f-14 top-10'>Ditawar Rp 200.000</p>
                    </div>
                    <div className='date ms-auto f-10'>
                        20 Apr, 14:04
                    </div>
                </div>

                <div className='btn-penawar'>
                    <button className='btn-tolak'>Tolak</button>
                    <button className='btn-terima' disabled={this.state.disabled} onClick={this.toggle}>{this.state.value}</button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}></ModalHeader>                                        
                    <ModalBody >
                    <h6>Yeay kamu berhasil mendapat harga yang sesuai</h6>
                    <br />
                    <p>Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</p>
                    <div className='product-match'>
                        <div className='product-tawar'>
                                <div>
                                <img src={seller} alt="" />
                                </div>
                                <div className='identitas'>
                                <p className='f-14'><strong>Nama Penjual</strong></p>
                                <p className='kota f-12'>kota</p>
                                </div>
                            </div>
                        <div class='product-tawar'>
                            <div>
                            <img src={clock1} alt="" width="60" height="60" class="br"/>
                            </div>
                            <div class='identitas'>
                                <p class='f-14'><strong>Jam Tangan Casio</strong></p>
                                <p >Rp 250.000</p>
                                <p>Ditawar Rp 200.000</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-outline pt-1 mb-2">
                        {/* <label class="form-label" for="form2Example11">Harga Tawar</label>  */}
                        <input type="hidden" id="" class="form-control"
                         onChange={this.handleChange}/>
                    </div>
                    <a href="/info-status">
                    <button class="btn btn-dark btn-block fa-lg mb-2 pt-2 pb-2" onClick={this.toggle}>Hubungi via Whatsapp <i class="bi bi-whatsapp"></i></button>
                    </a>
                    </ModalBody>
                    </Modal>    
                </div>
            </div>
            
        </div>
    </div>
    )
}}

export default InfoPenawar;