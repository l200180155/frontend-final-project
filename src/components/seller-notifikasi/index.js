import Form from "./Form"
import { useState, useEffect } from 'react'
import axios from 'axios'

const Notifikasi = () => {
    const [refetch, setRefetch] = useState({})

    const sellerNotifikasi = async (form) => {
        try {
            const response = await axios.post('http://localhost:3200/api/notifikasi', form)
            setRefetch(response.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div className=''>
            <nav className="navbar navbar-expand-lg">
            <div class="container-fluid">
                <a style={{fontFamily: 'Poppins'}} className="navbar-brand" href="#">
                 <img className="rectangle-navbar" style={{marginRight: 335}} src="rectangle.png" width={150} height={24} alt />
                </a>
                <form className="search-wrapper cf">
                    <input type="text" placeholder="Cari di sini ..." required style={{boxShadow: 'none'}} />
                    <button type="submit"><img className="search-control" src="search-control.png"></img></button>
                </form>
                <div className="collapse navbar-collapse icon-nav" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <a className="nav-link active" aria-current="page" href="#">
                            <img className="list-navbar icon-navbar" src="list-navbar.png"></img>
                        </a>
                        <a className="nav-link" href="#">
                            <img className="notifikasi-navbar icon-navbar" src="notifikasi-navbar.png"></img>
                        </a>
                        <a className="nav-link" href="#">
                            <img className="user-navbar icon-navbar" src="user-navbar.png"></img>
                        </a>
                    </div>
                </div>
            </div>
            </nav>
            <Form sellerNotifikasi={sellerNotifikasi}/>
        </div>
    )
}

export default Notifikasi