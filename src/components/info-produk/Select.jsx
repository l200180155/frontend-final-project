import React from 'react';

const Select = ({label, ...rest}) => {
    return (
        <div className="form-group mb-2">
        <p className='label'>{label}</p>
        <select required className="form-select mt-2" {...rest}>
            <option value="" disabled selected hidden>Pilih Kategori</option>
            <option value={1}>Hobi</option>
            <option value={2}>Kendaraan</option>
            <option value={3}>Baju</option>
            <option value={4}>Elektronik</option>
            <option value={5}>Kesehatan</option>
            <option value={6}>Lainya</option>
        </select>
    </div>
    )
}

export default Select